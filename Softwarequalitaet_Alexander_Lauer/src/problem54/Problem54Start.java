package problem54;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import datentypen.Duell;
import datentypen.Hand;
import datentypen.Karte;

import java.util.ArrayList;
import java.util.Scanner;


public class Problem54Start {
	
	/**
	 * readData liest die daten aus einer Datei ein
	 * @param input Datei aus der gelesen werden soll
	 * @return ArrayList mit allen duellen
	 */
	private static ArrayList<Duell>readData(RandomAccessFile input){
			ArrayList<Duell> duelle = new ArrayList <Duell>();
		try {
			input.seek(0);
			
			String eingabe = "";
			int eingabeLaenge = 0;
			Karte [] kartenS1 = new Karte [5];
			Karte [] kartenS2 = new Karte [5];
			int laengeKarten = kartenS1.length;
			Hand handSpieler1;
			Hand handSpieler2;
			int index1 = 0;
			int index2 = 0;
			Duell duell;
			
			while(input.length() > input.getFilePointer()){
				eingabe = input.readLine();
				index1 = 0;
				index2 = 0;
				kartenS2 = new Karte [5];
				kartenS1 = new Karte [5];
				
				eingabeLaenge = eingabe.length();
				for(int i = 0; i < eingabeLaenge -1; i = i +3){
					if(index1 < laengeKarten){
						kartenS1[index1] = new Karte(eingabe.charAt(i), eingabe.charAt(i+1)); 
						index1++;
					}else{
						kartenS2[index2] = new Karte(eingabe.charAt(i), eingabe.charAt(i+1)); 
						index2++;
					}
				}
				handSpieler1 = new Hand(kartenS1);
				handSpieler2 = new Hand(kartenS2);
				duell = new Duell(handSpieler1, handSpieler2);
				
				
				duelle.add(duell);
			}
			

			
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Fehler beim lesen der Daten");
			System.out.println("Programm wird beendet!!");
			System.exit(0);
		}
		return duelle;
		
	}
		
		
	
	
	/**Start des Problem54.
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("--- Problem 54 ---");
		System.out.println("Bitte Dateinamen/pfad eingeben: ");
		
		// Dateipfad einlesen
		
		Scanner scanner = new Scanner(System.in);
		final String DATEIPFAD = scanner.nextLine();
		scanner.close();
		try {
			final RandomAccessFile INPUT = new RandomAccessFile(DATEIPFAD, "rw");
			ArrayList<Duell> duelle= readData(INPUT);
			
			int spieler1Gewinnt = 0;
			
			for(Duell duell:duelle){
				if(duell.whoWins()){
					
					spieler1Gewinnt++;
				}
			}
			System.out.println("Spieler 1 hat " + spieler1Gewinnt + " mal gewonnen.");
			
		} catch (FileNotFoundException e) {
			System.out.println("Datei konnte nicht gefunden werden!!!");
			System.out.println("Programm wird beendet!!");
			System.exit(0);
		}
		
		
	}

}
