package datentypen;

/**
 * Duell repr�sentiert ein duell zwischen den H�nden zweier Spieler (bzw. die beiden H�nde,
 * die in einer Zeile Stehen) 
 * @author Alexander Lauer
 *
 */
public class Duell {
	public Hand handSpieler1;
	public Hand handSpieler2;
	
	/**
	 * Konstrukor 
	 * @param handSpieler1 Hand von Spieler 1
	 * @param handSpieler2 Hand von Spieler 2
	 */
	
	public Duell(final Hand HAND_SPIELER1,final Hand HAND_SPIELER2){
		this.handSpieler1 = HAND_SPIELER1;
		this.handSpieler2 = HAND_SPIELER2;
	}
	
	/**Getter- Methode f�r die Hand von Spieler 1
	 * 
	 * @return Die Hand von Spieler 1
	 */
	public Hand getHandSpieler1() {
		return handSpieler1;
	}
	
	
	/**Getter- Methode f�r die Hand von Spieler 1
	 * 
	 * @return Die Hand von Spieler 1
	 */
	public Hand getHandSpieler2() {
		return handSpieler2;
	}
	
	/**Entscheidet die Hand welches Spielers den vergleich gewinnt
	 * 
	 * @return true falls Spieler1 gewinnt, false falls Spieler2 gewinnt
	 */
	public boolean whoWins(){
		final int RANK_SPIELER_1 = handSpieler1.getRank();
		final int RANK_SPIELER_2 = handSpieler2.getRank();
		if(RANK_SPIELER_1 < RANK_SPIELER_2){
			return false;
		}
		//Bei Ranggleichheit werden alle F�lle gepr�ft
		if(RANK_SPIELER_1 == RANK_SPIELER_2){
			int highetsValueP1;
			int highetsValueP2;
			if(RANK_SPIELER_1 == 8 || RANK_SPIELER_1 == 5 || RANK_SPIELER_1 == 4 || RANK_SPIELER_1 == 2 || RANK_SPIELER_1 == 0){
				for(int i = 1; i < 5; i++){
					highetsValueP1 = handSpieler1.getHighestValue(i, 0);
					highetsValueP2  = handSpieler1.getHighestValue(i, 0);
					
					if(highetsValueP1  < highetsValueP2){
						return false;
					}
				}
			}
			if(RANK_SPIELER_1 == 7){
				highetsValueP1 = handSpieler1.getHighestValue(0, 4);
				highetsValueP2  = handSpieler1.getHighestValue(0, 4);
				
				if(highetsValueP1  < highetsValueP2){
					return false;
				}
				if(highetsValueP1  == highetsValueP2){
					for(int i = 1; i < 5; i++){
						highetsValueP1 = handSpieler1.getHighestValue(i, 0);
						highetsValueP2  = handSpieler1.getHighestValue(i, 0);
						
						if(highetsValueP1  < highetsValueP2){
							return false;
						}
					}
				}	
			}
			if(RANK_SPIELER_1 == 6){
				highetsValueP1 = handSpieler1.getHighestValue(0, 3);
				highetsValueP2  = handSpieler1.getHighestValue(0, 3);
				
				if(highetsValueP1  < highetsValueP2){
					return false;
				}
				if(highetsValueP1  == highetsValueP2){
					for(int i = 1; i < 5; i++){
						highetsValueP1 = handSpieler1.getHighestValue(i, 0);
						highetsValueP2  = handSpieler1.getHighestValue(i, 0);
						
						if(highetsValueP1  < highetsValueP2){
							return false;
						}
					}
				}	
			}
			if(RANK_SPIELER_1 == 3){
				highetsValueP1 = handSpieler1.getHighestValue(0, 3);
				highetsValueP2  = handSpieler1.getHighestValue(0, 3);
				
				if(highetsValueP1  < highetsValueP2){
					return false;
				}
				if(highetsValueP1  == highetsValueP2){
					for(int i = 1; i < 5; i++){
						highetsValueP1 = handSpieler1.getHighestValue(i, 0);
						highetsValueP2  = handSpieler1.getHighestValue(i, 0);
						
						if(highetsValueP1  < highetsValueP2){
							return false;
						}
					}
				}	
			}
			if(RANK_SPIELER_1 == 1){
				highetsValueP1 = handSpieler1.getHighestValue(0, 2);
				highetsValueP2  = handSpieler1.getHighestValue(0, 2);
				
				if(highetsValueP1  < highetsValueP2){
					return false;
				}
				if(highetsValueP1  == highetsValueP2){
					for(int i = 1; i < 5; i++){
						highetsValueP1 = handSpieler1.getHighestValue(i, 0);
						highetsValueP2  = handSpieler1.getHighestValue(i, 0);
						
						if(highetsValueP1  < highetsValueP2){
							return false;
						}
					}
				}	
			}
			
		}
		
		return true;
	}

	

}
