package datentypen;

/**
 * Card repr�sentiert eine Karte des Pokerblattes (z.B. Herz K�nig)
 * @author Alexander Lauer
 *
 */
public class Karte implements Comparable<Karte>{
	private char value;
	private char suit;
	
	/**
	 * 
	 * @param value der Wert der Karte
	 * @param suit Farbwert der farbe
	 */
	
	public Karte(final char VALUE,final char SUIT){
		this.value = VALUE;
		this.suit = SUIT;
	}
	/**
	 * Getter-Methode f�r Value
	 * @return Wert von value
	 */
	
	public char getValue() {
		return value;
	}
	/**
	 * Getter-Methode f�r Suit
	 * @return Wert von Suit
	 */
	
	public char getSuit() {
		return suit;
	}
	/**
	 * Vergleicht 2 Karten miteinander 
	 * @return -1 falls this < karte, 0 falls this = karte, 1 sonst
	 */
	public int compareTo(Karte karte) {
		final char VALUE_KARTE = karte.getValue(); 
		final boolean VALUE_KARTE2_IS_DIGIT =Character.isDigit(value);;
		final boolean VALUE_IS_DIGIT = Character.isDigit(value);
		
		if(VALUE_IS_DIGIT){
			if(value < VALUE_KARTE){
				return -1;
			}
			if(value > VALUE_KARTE){
				return 1;
			}
		}else{
			if(VALUE_KARTE2_IS_DIGIT){
				return 1;
			}
			if(value == VALUE_KARTE){
				return 0;
			}
			if(value == 'T'){
				if(VALUE_KARTE == 'J' || VALUE_KARTE == 'Q' || VALUE_KARTE == 'K' || VALUE_KARTE == 'A'){
					return -1;
				}
			}
			if(value == 'J'){
				if(VALUE_KARTE == 'Q' || VALUE_KARTE == 'K' || VALUE_KARTE == 'A'){
					return -1;
				}else{
					return 1;
				}
			}
			if(value == 'Q'){
				if(VALUE_KARTE == 'K' || VALUE_KARTE == 'A'){
					return -1;
				}else{
					return 1;
				}
			}
			if(value == 'K'){
				if(VALUE_KARTE == 'A'){
					return -1;
				}else{
					return 1;
				}
			}
		}
		return 0;
		
	}
	

}	