package datentypen;

import java.util.ArrayList;
import java.util.Iterator;

/**Hand repr�sentiert die Hand (bestehend aus 5 Karten) eines Spielers
 * 
 * @author Alexander Lauer
 *
 */

public class Hand {
	private Karte [] karten = new Karte [5];
	
	/**Kunstroktor der Klasse Hand
	 * 
	 * @param cards Array der gr��e 5 mit Elementen des Datentypes Card
	 */
	public Hand(final Karte [] KARTEN){
		this.karten = KARTEN;
	}
	
	/**
	 * getter-Methode f�r die Karten der Hand
	 * @return Die Karten der Hand 
	 */
	public Karte [] getCards(){
		return karten;
	}
	/**
	 * Tauscht die Karten an den positionen I J
	 * @param I Position der ersten zu tauschenden Karte
	 * @param J Position der zweiten zu tauschenden Karte
	 */
	private void tauschen(final int I, final int J){
		Karte t = karten [I];
		karten[I] = karten[J];
		karten[J] = t;
	}
	/**
	 * Sortiert die Karten anhand ihrer Wertigkeit
	 */
	private void sortKarten(){
		int kleinsteKarteIndex;
		for (int i = 0; i < 4; i++) {
			kleinsteKarteIndex = i;
			for (int j = i; j < 4; j++) {
				int k = karten[kleinsteKarteIndex].compareTo(karten[j]);
				if(k > 0){
					kleinsteKarteIndex = k;
				}
			}
			tauschen(kleinsteKarteIndex, i);
		}
	}
	/**
	 * Erzeugt einen Iterator mit Inhalt 2-A
	 * @return Ein Interator mit den M�glichen Werten einer KArte 
	 */
	private static Iterator<Character> getValueIterator(){
		ArrayList <Character> values = new ArrayList<Character>();
		values.add('2');
		values.add('3');
		values.add('4');
		values.add('5');
		values.add('6');
		values.add('7');
		values.add('8');
		values.add('9');
		values.add('T');
		values.add('J');
		values.add('Q');
		values.add('K');
		values.add('A');
		return values.iterator();
		
	}
	/**
	 * Pr�ft ob eine Hand i of a Kind ist (three of a Kind/ four of a Kind)
	 * @param i gibt an wie viele karten der gleichen Farbe vorhanden seien sollen. 
	 * @return true, falls es i karten mit gleicher Farbe gibt, sonst false
	 */
	private boolean ofAKind(final int I){
		
		int isDifferent = 0;
		int laenge = karten.length;
		boolean isOFAKind = true;
		char karte1Value;
		char karte2Value;
		for(int j = 0; j < laenge - 1; j++){
			karte1Value = karten[j].getValue();
			karte2Value = karten[j + 1].getValue();
			
			if(karte1Value != karte2Value){
				if(isDifferent == I){
					/*Verletzung der konvention, dass break vermieden werden soll.
					 *Ist isDifferent = i gibt es in der bereits i verschiedenen
					 *Suits. Kommt eine Weitere Karte mit anderer Suit dazu steht bereits fest, dass die 
					 *Hand kein i of a Kind ist. Es w�re also unn�tig (Zeitverschwendung) das Array 
					 *weiter zu durchlaufen
					*/
					isOFAKind = false;
					break;
				}else{
					isDifferent ++;
				}
			}
		}
		 return isOFAKind;
		
	}
	/**
	 * Pr�ft ob eine Hand ein  Flush ist
	 * @return true falls Hand ein Flush, sonst false
	 */
	
	private Boolean isFlush(){
		final int laenge = karten.length;
		final char SUIT = karten[0].getSuit();
		
		boolean isFlush = true;
		for (int i = 1; i < laenge; i++) {
			char suit = karten[i].getSuit();
			if(SUIT != suit){
				isFlush = false;
				//Wenn isflush = false gesetzt wird steht fest, dass die Han dkein Flush seien kann.
				// eine weiteres durchlaufen den Karen ist unn�tig
				break;
			}
		
		}
		return isFlush;
	}
	
	private boolean isStraight(){
		
		final int laenge = karten.length;
		final char VALUE = karten[0].getValue();
		Iterator<Character> iterator = getValueIterator();
		
		while(iterator.next() != VALUE){
			
		}
		
		boolean iteHasNext;
		boolean isStraight = true;
		for (int i = 1; i < laenge; i++) {
			iteHasNext = iterator.hasNext();
			if(iteHasNext){
				char value = karten[i].getValue();
				if(iterator.next() != value){
					isStraight = false;					
					//Wenn isStraught = false gesetzt wird steht fest, dass die Hand kein Straight seien kann.
					// eine weiteres durchlaufen den Karen ist unn�tig
					break;
				}
			}
		}
		
		return isStraight;
	}
	/**
	 * Gibt dem Wert der I h�chsten Karte der Hand zur�ck, oder den Wert der Karte, die ANZAHL mal vorkommt
	 * @param I 
	 * @param ANZAHL = 0 falls die I h�chste Karte gesucht ist, sonst die Anzahl der vorkommen der gesuchten karte 
	 * @return Wert der I- h�chsten karte (ANZHAL == 0) oder Wert der Karte deren Wert ANZAHL mal vorkommt
	 */
	
	public char getHighestValue(final int I, final int ANZAHL){
		sortKarten();
		int laenge = karten.length;
		if(ANZAHL == 0){
			return karten[laenge - I].getValue();
		}else{
			char karte1Value;
			char karte2Value;
			for(int i = 0; i < laenge - ANZAHL; i++){
				for(int j = 1; j < ANZAHL ; j++){
					karte1Value = karten[i].getValue();
					karte2Value = karten[i + j].getValue();
					if(karte1Value !=karte2Value){
						break;
					}
					if(karte1Value ==karte2Value && j == ANZAHL -1){
						return karte1Value;
					}
				}
			}
		}
		return 'z';
	}
	
	/**
	 * Ermittelt den rank der karten und gint diesen zur�ck
	 * @return eine Zahl zwischen 0 und 9, wobei 9 einen Royal Flush, 8 einen Straight Flush, ..., 0 eine High Card  repr�sentiert 
	 */
	public int getRank(){
		sortKarten();
		final char VALUE = karten[0].getValue();
		
		
		//Wenn Flush, dann Pr�fe ob Royal oder straight Flush
		if(isFlush() && isStraight()){
			if(VALUE == 'T'){
				return 9;
			}else{
				return 8;
			}
		}
		//Pr�fe ob Four of a Kind
		if(ofAKind(4)){
			return 7;
		}
		
		//Pr�fe ob Full House
		if(ofAKind(3) && ofAKind(2)){
			return 6;
		}
		//Pr�fe ob Flush
		if(isFlush()){
			return 5;
		}
		//Pr�fe ob Straight
		if(isStraight()){
			return 4;
		}
		//Pr�fe ob Three of a Kind
		if(ofAKind(3)){
			return 3;
		}
		//Pr�fe ob TwoPairs
		int laenge = karten.length;
		int pairs = 0;
		for(int i = 0; i < laenge - 1; i++){
			char value1 = karten[i].getValue();
			char value2 = karten[i + 1].getValue();
			if(value1 == value2){
				i++;
				pairs++;
			}
		}
		if(pairs == 2){
			return 2;
		}
		
		//Pr�fe ob Pair
		if(ofAKind(2)){
			return 1;
		}
		
		return 0;
	}
	
}
